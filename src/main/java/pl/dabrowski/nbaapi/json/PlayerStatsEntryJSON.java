package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class PlayerStatsEntryJSON {
    private PlayerJSON player;
    private PlayerTeamJSON team;
    private StatsJSON stats;

}
