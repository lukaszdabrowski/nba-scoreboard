package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class ScoreboardJSON {

    private int numGames;
    private GameJSON[] games;

}
