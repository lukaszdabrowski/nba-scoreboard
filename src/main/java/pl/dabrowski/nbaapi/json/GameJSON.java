package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class GameJSON {
    private String seasonYear;
    private String gameId;
    private ArenaJSON arena;
    private NuggetJSON nugget;
    private String attendance;
    private TeamJSON vTeam;
    private TeamJSON hTeam;





}
