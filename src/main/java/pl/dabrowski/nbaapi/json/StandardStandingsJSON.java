package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class StandardStandingsJSON {
    private ConferenceJSON conference;
}
