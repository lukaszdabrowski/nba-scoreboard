package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class ScoreJSON {
    private int score;
}
