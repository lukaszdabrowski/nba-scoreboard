package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class StatsJSON {
    private StatJSON GamesPlayed;
    private StatJSON Fg2PtPct;
    private StatJSON Fg3PtPct;
    private StatJSON FgPct;
    private StatJSON FtPct;
    private StatJSON RebPerGame;
    private StatJSON AstPerGame;
    private StatJSON PtsPerGame;
    private StatJSON StlPerGame;
    private StatJSON TovPerGame;
    private StatJSON BlkPerGame;
    private StatJSON FoulsPerGame;
    private StatJSON PlusMinusPerGame;
    private StatJSON GamesStarted;
}
