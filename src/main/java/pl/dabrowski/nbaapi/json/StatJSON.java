package pl.dabrowski.nbaapi.json;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

@Data
public class StatJSON {

    @SerializedName("@category")
    private String category;

    @SerializedName("@abbreviation")
    private String abbreviation;

    @SerializedName("#text")
    private String text;

}
