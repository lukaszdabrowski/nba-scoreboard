package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class TeamsJSON {
    private LeagueJSON league;
}
