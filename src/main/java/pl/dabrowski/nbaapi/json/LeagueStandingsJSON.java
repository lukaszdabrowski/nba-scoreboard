package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class LeagueStandingsJSON {
    private StandardStandingsJSON standard;
}
