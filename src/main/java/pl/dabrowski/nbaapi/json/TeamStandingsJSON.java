package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class TeamStandingsJSON {
    private String teamId;
    private String win;
    private String loss;
    private String winPct;
    private String homeWin;
    private String homeLoss;
    private String awayWin;
    private String awayLoss;
    private String lastTenWin;
    private String lastTenLoss;
    private String streak;
}
