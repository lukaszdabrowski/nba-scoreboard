package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class StatsEndpointJSON {
    private CumulativePlayerStatsJSON cumulativeplayerstats;
}
