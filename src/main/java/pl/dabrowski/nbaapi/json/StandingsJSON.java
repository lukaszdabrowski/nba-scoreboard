package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class StandingsJSON {
    private LeagueStandingsJSON league;

}
