package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class ConferenceJSON {
    private TeamStandingsJSON[] east;
    private TeamStandingsJSON[] west;
}
