package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class CumulativePlayerStatsJSON {
    private String lastUpdateOn;
    private PlayerStatsEntryJSON[] playerstatsentry;
}
