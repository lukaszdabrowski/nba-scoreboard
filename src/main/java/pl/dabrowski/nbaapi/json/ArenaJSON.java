package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class ArenaJSON {
    private String name;
    private Boolean isDomestic;
    private String city;
    private String stateAbbr;
    private String country;
}
