package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class PlayerJSON {
    private String ID;
    private String LastName;
    private String FirstName;
    private String JerseyNumber;
    private String Position;
}
