package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class TeamJSON {
    private String teamId;
    private String triCode;
    private String win;
    private String loss;
    private String seriesWin;
    private String seriesLoss;
    private String score;
}
