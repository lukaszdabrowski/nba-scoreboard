package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class StandardJSON {

    private boolean isNBAFranchise;
    private String city;
    private String fullName;
    private String tricode;
    private String teamId;
    private String nickname;
    private String confName;
    private String divName;
}
