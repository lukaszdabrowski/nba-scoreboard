package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class PlayerTeamJSON {
    private String ID;
    private String City;
    private String Name;
    private String Abbreviation;
}
