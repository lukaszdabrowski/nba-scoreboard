package pl.dabrowski.nbaapi.json;

import lombok.Data;

@Data
public class NuggetJSON {
    private String text;
}
