package pl.dabrowski.nbaapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NbaapiApplication {
    public static void main(String[] args) {
        SpringApplication.run(NbaapiApplication.class, args);
    }
}


