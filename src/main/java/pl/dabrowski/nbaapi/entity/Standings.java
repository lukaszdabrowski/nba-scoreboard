package pl.dabrowski.nbaapi.entity;

import lombok.Data;

//@Data
//@Entity
//@Table(name = "standings")
public class Standings {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String teamId;
    private String win;
    private String loss;
    private String winPct;
    private String homeWin;
    private String homeLoss;
    private String awayWin;
    private String awayLoss;
    private String lastTenWin;
    private String lastTenLoss;
    private String streak;
}
