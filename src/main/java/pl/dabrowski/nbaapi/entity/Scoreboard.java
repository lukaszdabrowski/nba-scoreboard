package pl.dabrowski.nbaapi.entity;

import lombok.Data;


//@Data
//@Entity
//@Table(name = "scoreboard")
public class Scoreboard {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String visitingTeam;
    private String visitingScore;
    private String homeScore;
    private String homeTeam;
    private String highlights;
}
