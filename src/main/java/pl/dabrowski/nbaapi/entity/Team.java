package pl.dabrowski.nbaapi.entity;

import lombok.Data;


//@Data
//@Entity
//@Table(name = "team")
public class Team {

//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private boolean isNBAFranchise;
    private String city;
    private String fullName;
    private String tricode;
    private String teamId;
    private String nickname;
    private String confName;
    private String divName;
}
