package pl.dabrowski.nbaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.dabrowski.nbaapi.json.PlayerStatsEntryJSON;
import pl.dabrowski.nbaapi.service.StatsService;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.toMap;

@Controller
@RequestMapping("/stats")
public class StatsController {

    @Value("Current NBA Statistics")
    String pageName;

    @Autowired
    StatsService statsService;

    @Value("${spring.application.name}")
    String appName;

    String appMode;

    @Autowired
    public StatsController (Environment environment){
        appMode = environment.getProperty("app-mode");
    }


    @GetMapping
    public String homePage(Model model) throws IOException {
        model.addAttribute("appName", appName);
        model.addAttribute("pageName",pageName);
        PlayerStatsEntryJSON[] players = statsService.getStats();
        Map<String, Double> pointsRaw = new HashMap<String, Double>();
        for (PlayerStatsEntryJSON p:players) {
            pointsRaw.put(p.getPlayer().getFirstName() + ' ' + p.getPlayer().getLastName(), Double.parseDouble(p.getStats().getPtsPerGame().getText()));
        }
        Map<String, Double> points = pointsRaw.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .limit(20).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        Map<String, Double> rebsRaw = new HashMap<String, Double>();
        for (PlayerStatsEntryJSON p:players) {
            pointsRaw.put(p.getPlayer().getFirstName() + ' ' + p.getPlayer().getLastName(), Double.parseDouble(p.getStats().getRebPerGame().getText()));
        }
        Map<String, Double> rebs = pointsRaw.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .limit(20).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));


        Map<String, Double> assistsRaw = new HashMap<String, Double>();
        for (PlayerStatsEntryJSON p:players) {
            pointsRaw.put(p.getPlayer().getFirstName() + ' ' + p.getPlayer().getLastName(), Double.parseDouble(p.getStats().getAstPerGame().getText()));
        }
        Map<String, Double> assists = pointsRaw.entrySet().stream()
                .sorted(Map.Entry.<String, Double>comparingByValue().reversed())
                .limit(20).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));




        model.addAttribute("points",points);
        model.addAttribute("rebs",rebs);
        model.addAttribute("assists",assists);
        return "stats";
    }

}
