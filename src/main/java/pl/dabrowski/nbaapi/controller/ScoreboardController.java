package pl.dabrowski.nbaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.dabrowski.nbaapi.json.*;
import pl.dabrowski.nbaapi.service.ScoreBoardService;

import java.io.IOException;

@Controller
@RequestMapping("/")
public class ScoreboardController {


    @Value("${spring.application.name}")
    String appName;

    String appMode;

    @Autowired
    public ScoreboardController (Environment environment){
        appMode = environment.getProperty("app-mode");
    }

    @Autowired
    private ScoreBoardService scoreBoardService;

    @GetMapping
    public void getScoreboard() throws IOException {
        ScoreboardJSON scoreboardJSON = scoreBoardService.fetchScoreboard();
        GameJSON[] games = scoreboardJSON.getGames();
        for (GameJSON g : games) {
            System.out.println(g.getVTeam().getTriCode() + " " + g.getVTeam().getScore() + " - " + g.getHTeam().getTriCode() + " " + g.getHTeam().getScore());
            System.out.println(g.getNugget().getText());
        }
    }

    @GetMapping("/start")
    public String homePage(Model model) throws IOException {
        model.addAttribute("appName", appName);
//        StandingsJSON standings = standingsService.fetchStandings();
//        ConferenceJSON conferenceJSON = standings.getLeague().getStandard().getConferenceJSON();
//        TeamStandingsJSON[] east = conferenceJSON.getEast();
//        TeamStandingsJSON[] west = conferenceJSON.getWest();
//        for(TeamStandingsJSON t: east) {
//            System.out.println(t.getTeamId());
//            System.out.println(t.getWinPct());
//        }
        return "home";
    }

    @GetMapping("/scores")
    public String scorePage(Model model) throws IOException {
        ScoreboardJSON scoreboardJSON = scoreBoardService.fetchScoreboard();
        GameJSON[] games = scoreboardJSON.getGames();
        model.addAttribute("appName", appName);
        model.addAttribute("gameResults",games);
        return "scores";
    }

}

