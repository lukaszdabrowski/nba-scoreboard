package pl.dabrowski.nbaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.dabrowski.nbaapi.json.ConferenceJSON;
import pl.dabrowski.nbaapi.json.StandingsJSON;
import pl.dabrowski.nbaapi.json.TeamStandingsJSON;
import pl.dabrowski.nbaapi.service.StandingsService;
import pl.dabrowski.nbaapi.service.TeamsService;

import java.io.IOException;
import java.util.Map;

@Controller
@RequestMapping("/standings")
public class StandingsController {

    @Value("Current NBA Standings")
    String pageName;

    @Autowired
    private StandingsService standingsService;

    @Autowired
    private TeamsService teamsService;

    @GetMapping
    public String homePage (Model model) throws IOException {
        StandingsJSON standings = standingsService.fetchStandings();
        ConferenceJSON conference = standings.getLeague().getStandard().getConference();
        TeamStandingsJSON[] east = conference.getEast();
        TeamStandingsJSON[] west = conference.getWest();
        Map<String,String> teamNames = teamsService.getTeamNames();
        model.addAttribute("pageName",pageName);
        model.addAttribute("east",east);
        model.addAttribute("west",west);
        model.addAttribute("teamNames",teamNames);
        return "standings";
    }

}
