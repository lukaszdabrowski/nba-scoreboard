package pl.dabrowski.nbaapi.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.dabrowski.nbaapi.json.*;
import pl.dabrowski.nbaapi.service.TeamsService;

import java.io.IOException;

@Controller
@RequestMapping("/teams")
public class TeamsController {

    @Autowired
    private TeamsService teamsService;

    @GetMapping
    public String homePage(Model model) throws IOException {
        TeamsJSON teams = teamsService.fetchTeams();
        LeagueJSON league = teams.getLeague();
        StandardJSON[] standards = league.getStandard();
        model.addAttribute("standards",standards);
        return "teams";
    }

}
