package pl.dabrowski.nbaapi.dto;

import lombok.Data;

@Data
public class ScoreboardDTO {
    private Long id;
    private String visitingTeam;
    private String visitingScore;
    private String homeScore;
    private String homeTeam;
    private String highlights;



}
