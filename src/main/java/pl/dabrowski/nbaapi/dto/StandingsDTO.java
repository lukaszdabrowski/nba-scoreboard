package pl.dabrowski.nbaapi.dto;

import lombok.Data;

@Data
public class StandingsDTO {
    private Long id;
    private String teamId;
    private String win;
    private String loss;
    private String winPct;
    private String homeWin;
    private String homeLoss;
    private String awayWin;
    private String awayLoss;
    private String lastTenWin;
    private String lastTenLoss;
    private String streak;
}
