package pl.dabrowski.nbaapi.dto;

import lombok.Data;

@Data
public class PlayerDTO {
    private Long id;

    private String teamCity;
    private String teamName;
    private String teamAbbreviation;
    private String LastName;
    private String FirstName;
    private String JerseyNumber;
    private String Position;
    private String GamesPlayed;
    private String Fg2PtPct;
    private String Fg3PtPct;
    private String FgPct;
    private String FtPct;
    private String RebPerGame;
    private String AstPerGame;
    private String PtsPerGame;
    private String StlPerGame;
    private String TovPerGame;
    private String BlkPerGame;
    private String FoulsPerGame;
    private String PlusMinusPerGame;
    private String GamesStarted;
}
