package pl.dabrowski.nbaapi.dto;

import lombok.Data;

@Data
public class TeamDTO {
    private Long id;
    private boolean isNBAFranchise;
    private String city;
    private String fullName;
    private String tricode;
    private String teamId;
    private String nickname;
    private String confName;
    private String divName;
}
