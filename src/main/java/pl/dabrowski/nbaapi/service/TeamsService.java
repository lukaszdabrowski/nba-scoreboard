package pl.dabrowski.nbaapi.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.dabrowski.nbaapi.json.StandardJSON;
import pl.dabrowski.nbaapi.json.TeamsJSON;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Service
public class TeamsService {

    private final RestTemplate restTemplate;

    @Autowired
    private Gson gson;

    @Autowired
    public TeamsService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public TeamsJSON fetchTeams() throws IOException {
        String teamsString = restTemplate.getForObject("http://data.nba.net/10s/prod/v2/2018/teams.json",String.class);
        TeamsJSON teams = gson.fromJson(teamsString, TeamsJSON.class);
        return teams;

    }

    public Map<String,String> getTeamNames() {
        Map<String, String> teamNames = new HashMap<String, String>();
        String teamsString = restTemplate.getForObject("http://data.nba.net/10s/prod/v2/2018/teams.json",String.class);
        TeamsJSON teams = gson.fromJson(teamsString, TeamsJSON.class);
        StandardJSON[] standards = teams.getLeague().getStandard();
        for (StandardJSON s:standards) {
            teamNames.put(s.getTeamId(),s.getFullName());
        }
        return teamNames;
    }

}
