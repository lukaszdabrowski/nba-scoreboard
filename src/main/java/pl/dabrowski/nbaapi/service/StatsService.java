package pl.dabrowski.nbaapi.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.dabrowski.nbaapi.json.CumulativePlayerStatsJSON;
import pl.dabrowski.nbaapi.json.PlayerStatsEntryJSON;
import pl.dabrowski.nbaapi.json.StatsEndpointJSON;

@Service
public class StatsService {

    private final RestTemplate restTemplate;

    @Autowired
    private Gson gson;

    @Autowired
    public StatsService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.basicAuthorization("c31aa58e-a972-41af-8081-2e7319","picard").build();
    }

    public PlayerStatsEntryJSON[] getStats() {
        String statString = restTemplate.getForObject("https://api.mysportsfeeds.com/v1.0/pull/nba/2018-2019-regular/cumulative_player_stats.json", String.class);
        StatsEndpointJSON stats = gson.fromJson(statString, StatsEndpointJSON.class);
        CumulativePlayerStatsJSON cumulativeplayerstats = stats.getCumulativeplayerstats();
        PlayerStatsEntryJSON[] players = cumulativeplayerstats.getPlayerstatsentry();
        return players;

    }

//    public void updatePlayerDatabase() {
//        String statString = restTemplate.getForObject("https://api.mysportsfeeds.com/v1.0/pull/nba/2018-2019-regular/cumulative_player_stats.json", String.class);
//        StatsEndpointJSON stats = gson.fromJson(statString, StatsEndpointJSON.class);
//        CumulativePlayerStatsJSON cumulativeplayerstats = stats.getCumulativeplayerstats();
//        PlayerStatsEntryJSON[] players = cumulativeplayerstats.getPlayerstatsentry();
//        for (PlayerStatsEntryJSON p:players) {
//            PlayerDTO playerDTO = new PlayerDTO();
//        }
//    }
//
//    public List<PlayerDTO> findAll() {
//        List<Player> players = playerRepository.findAll();
//        return players.stream().map(this::mapTo).collect(Collectors.toList());
//    }
//
//    public PlayerDTO findOne(Long id) {
//        Player player = playerRepository.findById(id)
//                .orElseThrow(() -> new EntityNotFoundException(""));
//        return mapTo(player);
//    }
//
//    public void create(PlayerDTO dto) {
//        Player player = mapTo(dto);
//        playerRepository.save(player);
//    }
//
//    public void update(Long id, PlayerDTO dto) {
//        Player player = mapTo(dto);
//        playerRepository.save(player);
//    }
//
//    public void delete(Long id) {
//        playerRepository.deleteById(id);
//    }
//
//    private PlayerDTO mapTo(Player player) {
//        PlayerDTO dto = new PlayerDTO();
//        dto.setId(player.getId());
//        dto.setTeamCity(player.getTeamCity());
//        dto.setTeamName(player.getTeamName());
//        dto.setTeamAbbreviation(player.getTeamAbbreviation());
//        dto.setLastName(player.getLastName());
//        dto.setFirstName(player.getFirstName());
//        dto.setJerseyNumber(player.getJerseyNumber());
//        dto.setPosition(player.getPosition());
//        dto.setGamesPlayed(player.getGamesPlayed());
//        dto.setFg2PtPct(player.getFg2PtPct());
//        dto.setFg3PtPct(player.getFg3PtPct());
//        dto.setFgPct(player.getFgPct());
//        dto.setFtPct(player.getFtPct());
//        dto.setRebPerGame(player.getRebPerGame());
//        dto.setAstPerGame(player.getAstPerGame());
//        dto.setPtsPerGame(player.getPtsPerGame());
//        dto.setStlPerGame(player.getStlPerGame());
//        dto.setTovPerGame(player.getTovPerGame());
//        dto.setBlkPerGame(player.getBlkPerGame());
//        dto.setFoulsPerGame(player.getFoulsPerGame());
//        dto.setPlusMinusPerGame(player.getPlusMinusPerGame());
//        dto.setGamesStarted(player.getGamesStarted());
//        return dto;
//    }
//
//    private Player mapTo(PlayerDTO dto) {
//        Player player = new Player();
//        player.setId(dto.getId());
//        player.setTeamCity(dto.getTeamCity());
//        player.setTeamName(dto.getTeamName());
//        player.setTeamAbbreviation(dto.getTeamAbbreviation());
//        player.setLastName(dto.getLastName());
//        player.setFirstName(dto.getFirstName());
//        player.setJerseyNumber(dto.getJerseyNumber());
//        player.setPosition(dto.getPosition());
//        player.setGamesPlayed(dto.getGamesPlayed());
//        player.setFg2PtPct(dto.getFg2PtPct());
//        player.setFg3PtPct(dto.getFg3PtPct());
//        player.setFgPct(dto.getFgPct());
//        player.setFtPct(dto.getFtPct());
//        player.setRebPerGame(dto.getRebPerGame());
//        player.setAstPerGame(dto.getAstPerGame());
//        player.setPtsPerGame(dto.getPtsPerGame());
//        player.setStlPerGame(dto.getStlPerGame());
//        player.setTovPerGame(dto.getTovPerGame());
//        player.setBlkPerGame(dto.getBlkPerGame());
//        player.setFoulsPerGame(dto.getFoulsPerGame());
//        player.setPlusMinusPerGame(dto.getPlusMinusPerGame());
//        player.setGamesStarted(dto.getGamesStarted());
//        return player;
    }

