package pl.dabrowski.nbaapi.service;


import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.dabrowski.nbaapi.json.StandingsJSON;

import java.io.IOException;

@Service
public class StandingsService {

    private final RestTemplate restTemplate;

    @Autowired
    private Gson gson;

    @Autowired
    public StandingsService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public StandingsJSON fetchStandings() throws IOException {
        String teamsString = restTemplate.getForObject("http://data.nba.net/10s/prod/v1/current/standings_conference.json", String.class);
        StandingsJSON standings = gson.fromJson(teamsString, StandingsJSON.class);
        return standings;

    }
}
