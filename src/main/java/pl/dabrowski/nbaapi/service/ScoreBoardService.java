package pl.dabrowski.nbaapi.service;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;
import pl.dabrowski.nbaapi.json.ScoreboardJSON;

import java.io.IOException;
import java.net.URI;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service
public class ScoreBoardService {

    private final  RestTemplate restTemplate;

    @Autowired
    private Gson gson;

    @Autowired
    public ScoreBoardService(RestTemplateBuilder restTemplateBuilder) {
        restTemplate = restTemplateBuilder.build();
    }

    public ScoreboardJSON fetchScoreboard() throws IOException {

        DateTimeFormatter formatter = DateTimeFormatter.BASIC_ISO_DATE;
        String date = LocalDate.now().minusDays(1).format(formatter);
        UriComponents uriComponents =
                UriComponentsBuilder.newInstance()
                        .scheme("http")
                        .host("data.nba.net")
                        .path("/10s/prod/v1/{date}/scoreboard.json")
                        .build()
                        .expand(date)
                        .encode();
        URI uri = uriComponents.toUri();

        String scoreboardString = restTemplate.getForObject(uri,String.class);
        ScoreboardJSON scoreboardJSON = gson.fromJson(scoreboardString, ScoreboardJSON.class);
        return scoreboardJSON;

    }


}

